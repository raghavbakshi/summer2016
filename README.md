# Programming Assignment

Write a python program that reads a raster data file and calculates 
the average cell value for any band in the raster file. 

Do the calculation these two ways:

1. Include cell with a value of 0 in the calculation
2. Ignore cell with a value of 0 in the calculation

Basically the only difference between these is what you divide by, so you
only need to create one total of all cell values. But while you’re 
totaling cell values, you should also count the number of non-zero cells. 

This should be a standard structured program that handles command line
arguments in a standard way and produces a nice usage statement. Arguments
should allow the user to choose a file, band, and method of calculation.

### Recommended Modules:
argparse
gdal


